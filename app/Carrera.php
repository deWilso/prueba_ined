<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    //
    protected $table = 'carrera';
    protected $fillable = [
        'id', 'nombre', 'jornada_id'
    ];

    public function jornadas(){
        return $this->belongsTo('App\Jornada', 'jornada_id');
    }
    
    public function grados(){
        return $this->hasMany('App\Grado');
    }
}
