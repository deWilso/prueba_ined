<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table ='grado';
    protected $fillable =[
        'id', 'nombre', 'carrera_id'
    ];

    public function carreras () {

        return $this->belongsTo('App\Carrera', 'carrera_id');
    }
    

    public function cursos(){
        return $this->hasMany('App\Curso');
    }
    

    public function estudiantes(){
        return $this->hasMany('App\Estudiantes');
    }
    
}
