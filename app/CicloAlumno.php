<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CicloAlumno extends Model
{
    protected $table = 'cicloAlumno';
    protected $fillable = [
        'id', 'estudiante_id', 'ciclo_id'
    ];

    public function ciclo(){
        return $this->belongsTo('App\Ciclo', 'ciclo_id');
    }
    public function estudiante(){
        return $this->belongsTo('App\Estudiante', 'estudiante_id');
    }
  
}
