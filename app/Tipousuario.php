<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipousuario extends Model
{
    protected $table = 'tipousuarios';
    
    protected $fillable = [
        'id', 'tipo'
    ];

    public function users(){
        return $this->hasMany('App\User');
    }
    
}
