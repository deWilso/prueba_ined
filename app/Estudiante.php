<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    //
    protected $table = 'estudiante';
    protected $fillable = [
        'id', 'carnet','pNombre', 'sNombre','tNombre', 'pApellido', 'sApellido', 'aCasada', 'direccion', 
        'telefono', 'fechaNacimiento','grado_id', 'responsable_id', 'genero_id', 'carrera_id'
    ];

   
    public function generos(){
        return $this->belongsTo('App\Genero', 'genero_id');
    }
    public function asignaciones(){
        return $this->hasMany('App\Asignacion');
    }

    public function grados (){
        return $this->belongsTo('App\Grado', 'grado_id');
    }
    public function ciclos(){

        return $this->hasMany('App\CicloAlumno');
    }
}
