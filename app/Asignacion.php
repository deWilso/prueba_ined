<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    //
    protected $table = 'asignacion';
    protected $fillable =[
        'id', 'estudiante_id', 'curso_id'
    ];

    public function cursos (){

        return $this->belongsTo('App\Curso');
    }
    public function estudiantes (){
        return $this->belongsTo('App\Estudiante');
    }
}
