<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciclo extends Model
{
    protected $table = 'ciclo';
    protected $fillable = [
        'id', 'ciclo', 'estado_id'
    ];

    public function estadoCiclo(){
        return $this->belongsTo('App\EstadoCiclo', 'estado_id');
    }
    
    public function cicloAlumno(){
        return $this->hasMany('App\CicloAlumno');
    }
}
