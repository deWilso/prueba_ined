<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCiclo extends Model
{
    protected $table = 'estadoCiclo';
    protected $fillable = [
        'id', 'estado'
    ];

    public function ciclos(){
        return $this->hasMany('App\ciclo', 'ciclo_id');
    }
    
}
