<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Grado;
use App\Carrera;
use App\User;
class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $grados = Grado::all();
      $cursos = Curso::orderBy('id', 'ASC')->paginate(10);
      $docentes = User::all();
      $carreras = Carrera::all();
      return view ("cursos.lista")->with('grados', $grados)
                                ->with('cursos', $cursos)
                                ->with('docentes', $docentes)
                                ->with('carreras', $carreras);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nombreCurso = $request->curso;
        $docente = $request->docente;
        $grado = $request->grado;

        $nuevoCurso = New Curso;
        $nuevoCurso->nombre = $nombreCurso;
        $nuevoCurso->grado_id = $grado;
        $nuevoCurso->usuario_id = $docente;

        $nuevoCurso->save();
        return redirect(route('cursoList'))->with('status', 'Curso '.$nombreCurso.' creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request   $request)
    {
        if($request->ajax())
                {
                    $id=$request->id;
                   
                    $curso= Curso::whereId($id)->get();
                    return Response($curso);
                }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = Curso::whereId($id)->firstOrFail();
        $curso->nombre = $request->get('curso');
        $curso->grado_id= $request->get('grado');
        $curso->usuario_id = $request->get('docente');
        
        $curso->save();
        return redirect(route('cursoList'))->with('status', 'Curso'.$curso->nombre.' Actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curso = Curso::whereId($id)->firstOrFail();
        $curso->delete();
        return redirect(route('cursoList'))->with('status', 'Curso '.$curso->nombre.' eliminado con exito');
    }
}
