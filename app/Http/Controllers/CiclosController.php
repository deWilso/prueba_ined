<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciclo;
use App\EstadoCiclo;

class CiclosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ciclos = Ciclo::whereEstado_id(1)->get();
        return view ('ciclos.ciclos')->with('ciclos', $ciclos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cerrarCiclo(Request $request, $id)
    {
      $ciclo = Ciclo::whereId($id)->firstOrFail();
      $ciclo->estado_id = 2; //con los valores 1 o 2 se cambia de abierto a cerrado el ciclo respectivamente
      $ciclo->save();
      return redirect(route('cicloEscolar'))->with('status', 'Ciclo '.$ciclo->ciclo.' Cerrado con exito');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ciclo = $request->cicloEscolar;
        $estadoCiclo = 1;

        $nuevoCiclo = New Ciclo;
        $nuevoCiclo->ciclo = $ciclo;
        $nuevoCiclo->estado_id = $estadoCiclo;
        $nuevoCiclo->save();

        return redirect(route('cicloEscolar'))->with('status', 'Ciclo '.$ciclo.' creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
