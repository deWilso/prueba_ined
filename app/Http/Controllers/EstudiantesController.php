<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator,Redirect,Response;
use App\Estudiante;
use App\Genero;
use App\Grado;
use App\Carrera;
use App\Curso;
use App\Asignacion;
use App\Ciclo;
use App\CicloAlumno;
class EstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $cicloEscolar = Ciclo::select('id', 'ciclo','estado_id')->orderBy('id', 'DESC')->first();
        $estudiantes = Estudiante::orderBy('id', 'ASC')->paginate(10);
        
        // $alumnos = DB::select('select a.id as "asignacion", e.id, e.pNombre, e.pApellido from estudiante as e
        // join asignacion as a on e.id = a.estudiante_id join curso as c on c.id = a.curso_id where a.curso_id='. $cursoId);
        
        return view ('estudiantes.lista')->with('estudiantes', $estudiantes);
                                       
                                         
                                        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grados = Grado::all();
        $genero = Genero::all();
        $carreras = Carrera::all();
        return view('estudiantes.create')->with('generos', $genero)
        ->with('grados', $grados)
        ->with('carreras', $carreras);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                 
        
        $cicloEscolar = Ciclo::select('id', 'ciclo','estado_id')->orderBy('id', 'DESC')->first();
        if ($cicloEscolar->estado_id==1)
        {       
            $carnetEstudiate = $request->carnet;
            $dpiEstudiante = $request->dpiEst;
            $pNombreEstudiante = $request->pNombreEst;
            $sNombreEstudiante = $request->sNombreEst;
            $tNombreEstudiante = $request->tNombreEst;
            $pApellidoEstudiante= $request->pApellidoEst;
            $sApellidoEstudiante= $request->sApellidoEst;
            $aCasadaEstudiante= $request->aCasadaEst;
            $direccionEstudiante= $request->direccionEst;
            $telEstudiante= $request->telEstudiante;
            $fechaNacimiento= $request->fechaNac;
            $genero = $request->genero;
            $grado = $request->grado;
            $establecimientoAnterior = $request->estabAnterior;
               
            $nuevoEstudiante = new Estudiante;
            $nuevoEstudiante->carnet = $carnetEstudiate;
            $nuevoEstudiante->pNombre = $pNombreEstudiante;
            $nuevoEstudiante->sNombre = $sNombreEstudiante;
            $nuevoEstudiante->tNombre = $tNombreEstudiante;
            $nuevoEstudiante->pApellido = $pApellidoEstudiante;
            $nuevoEstudiante->sApellido = $sApellidoEstudiante;
            $nuevoEstudiante->aCasada = $aCasadaEstudiante;
            $nuevoEstudiante->direccion = $direccionEstudiante;
            $nuevoEstudiante->telefono = $telEstudiante;
            $nuevoEstudiante->fechaNacimiento = $fechaNacimiento;
            $nuevoEstudiante->dpi =$dpiEstudiante;
            $nuevoEstudiante->genero_id = $genero;
            $nuevoEstudiante->grado_id = $grado;
            $nuevoEstudiante->estabAnterior = $establecimientoAnterior;
            $nuevoEstudiante->save();

            $cicloAlumno = new CicloAlumno;
            $cicloAlumno->estudiante_id = $nuevoEstudiante->id; 
            $cicloAlumno->ciclo_id = $cicloEscolar->id;
            $cicloAlumno->save();
            
            $cursos = Curso::whereGrado_id($grado)->get();    
            if ($cursos)
            {
                foreach ($cursos as $curso)
                {
                    $estudiante = $nuevoEstudiante->id;
                    $nuevaAsignacion = new Asignacion;
                    $nuevaAsignacion->curso_id = $curso->id;    
                    $nuevaAsignacion->estudiante_id = $estudiante;
                    $nuevaAsignacion->save();                    

                }           

		        return redirect(route('estudianteList'))->with('status', 'Alumno Incrito Exitosamente');
		   
            } else 
            {                 
		        return redirect(route('estudianteList'))->with('status', ' No se ha podido registrar estudiante');
            }          
                    
        } else {
            return redirect(route('estudianteList'))->with('status', 'No hay ciclo escolar abierto, favor aperturar ciclo');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
