<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jornada;
use App\Carrera;
class CarrerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carreras = Carrera::orderBy('id', 'ASC')->paginate(10);;
        $jornadas = Jornada::all();
        return view('carreras.lista')->with('jornadas', $jornadas)
                                    ->with('carreras', $carreras);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jornada = $request->jornada;
        $carrera = $request->nombreCarrera;

        $nuevaCarrera = new Carrera;
        $nuevaCarrera->nombre = $carrera;
        $nuevaCarrera->jornada_id=$jornada;
        $nuevaCarrera->save();
        return redirect(route('carreraList'))->with('status', 'Carrera '.$carrera.' creada con exito');

    }
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if($request->ajax())
                {
                    $id=$request->id;
                   
                    $carrera= Carrera::whereId($id)->get();
                    return Response($carrera);
                }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carrera = Carrera::whereId($id)->firstOrFail();
        $carrera->nombre = $request->get('nombreCarrera');
        $carrera->jornada_id= $request->get('jornada');
        
        $carrera->save();
        return redirect(route('carreraList'))->with('status', 'Carrera'.$carrera->nombre.' Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carrera = Carrera::whereId($id)->firstOrFail();
        $carrera->delete();
        return redirect(route('carreraList'))->with('status', 'Carrera '.$carrera->nombre.' eliminada con exito');
    }
}
