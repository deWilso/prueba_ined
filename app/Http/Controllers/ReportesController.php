<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grado;
use App\Carrera;
class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generaReporteGrado()
    {
        $carreras = Carrera::all();
        return view('reportes.generaReporteGrado')->with('carreras', $carreras);       

    }

    public function reporteCalificacionesDeGrado(Request $request)
    {
        $gradoId = $request->grado;
        $cicloEscolar = Ciclo::select('id', 'ciclo','estado_id')->orderBy('id', 'DESC')->first();
        $alumnos = DB::select("select a.id as 'asignacion', e.id, e.pNombre, e.pApellido from estudiante as e
        join asignacion as a on e.id = a.estudiante_id join curso as c on c.id = a.curso_id join cicloalumno as ca on ca.estudiante_id = e.id where ca.ciclo_id = '$cicloEscolar->id'and a.curso_id = ".$cursoId);

        return view ('reportes.reporteGrado');

    }

    public function reporteCalificacionesEstudiante(Request $request)
    {
     
        return view ('reportes.reporteEstudiante');

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
