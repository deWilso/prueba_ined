<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grado;
use App\Carrera;
class GradosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grados = Grado::orderBy('id', 'ASC')->paginate(10);;
        $carreras = Carrera::all();
        return view('grados.lista')->with('grados', $grados)
                                    ->with('carreras', $carreras);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grado = $request->nombreGrado;
        $carrera = $request->carrera;

        $nuevoGrado = new Grado;
        $nuevoGrado->nombre = $grado;
        $nuevoGrado->carrera_id=$carrera;
        $nuevoGrado->save();
        return redirect(route('gradoList'))->with('status', 'Grado '.$grado.' creado con exito');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getGrados(Request $request)
    {
        if($request->ajax())
        {
            $id=$request->id;
           
            $carrera= Grado::whereCarrera_id($id)->get();
            return Response($carrera);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if($request->ajax())
                {
                    $id=$request->id;
                   
                    $grado= Grado::whereId($id)->get();
                    return Response($grado);
                }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grado = Grado::whereId($id)->firstOrFail();
        $grado->nombre = $request->get('nombreGrado');
        $grado->carrera_id= $request->get('carrera');
        
        $grado->save();
        return redirect(route('gradoList'))->with('status', 'Grado'.$grado->nombre.' Actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grado = Grado::whereId($id)->firstOrFail();
        $grado->delete();
        return redirect(route('GradoList'))->with('status', 'Grado '.$grado->nombre.' eliminado con exito');
    }
}
