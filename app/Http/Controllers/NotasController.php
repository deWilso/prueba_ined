<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Carrera;
use App\Curso;
use App\User;
use App\Estudiante;
use App\Unidad;
use App\Nota;
use App\Ciclo;
use Illuminate\Support\Facades\DB;
class NotasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function panelBusquedaCurso () 
    {
        $idDocente = auth()->id();
        $usuario = User::whereId($idDocente)->get();
        foreach ($usuario as $tipo){

            $tipoUsuarioId= $tipo->tipousuarios_id;
        }
        $unidades = Unidad::all(); 
        if ($tipoUsuarioId!=1){
            $cursos = Curso::whereUsuario_id($idDocente)->get();
            return view('notas.notaCursoCreate')->with('cursos', $cursos)
                                            ->with('unidades', $unidades);
        }
        else {
            $cursos = Curso::all();
            return view('notas.notaCursoCreate')->with('cursos', $cursos)
            ->with('unidades', $unidades);
        }

        
        
    }


    public function getAlumnosCurso (Request $request)
    {

        if($request->ajax())
        {
            $prueba = 1;
            $cursoId=$request->id;
            $cicloEscolar = Ciclo::select('id', 'ciclo','estado_id')->orderBy('id', 'DESC')->first();
            $alumnos = DB::select("select a.id as 'asignacion', e.id, e.pNombre, e.pApellido from estudiante as e
            join asignacion as a on e.id = a.estudiante_id join curso as c on c.id = a.curso_id join cicloalumno as ca on ca.estudiante_id = e.id where ca.ciclo_id = '$cicloEscolar->id'and a.curso_id = ".$cursoId);
            
            return Response($alumnos);
        }
               
    }

    
    public function panelBusquedaAlumno ()  
    {
        $id = auth()->id();
        $usuario = User::whereId($id)->get();
        foreach ($usuario as $tipo){

            $tipoUsuarioId= $tipo->tipousuarios_id;
        }
        $carreras = Carrera::all();
        $unidades = Unidad::all();
        $cicloEscolar = Ciclo::select('id', 'ciclo','estado_id')->orderBy('id', 'DESC')->first();
        if ($tipoUsuarioId!=1) {
        $cursos = Curso::whereUsuario_id($id)->get();        
        $estudiantes = DB::select("SELECT DISTINCT e.carnet, e.id, e.pNombre,   e.pApellido, e.direccion from estudiante as e 
        inner join curso as c on c.grado_id = e.grado_id join cicloalumno as ca on ca.estudiante_id = e.id where ca.ciclo_id =' $cicloEscolar->id ' and c.usuario_id =' $id'");
        return view('notas.notaAlumnoCreate')->with('carreras', $carreras)
        ->with('estudiantes', $estudiantes) 
        ->with('unidades', $unidades);
        }
        else {
            $cursos= Curso::all();
            $estudiantes = DB::select('SELECT DISTINCT e.carnet, e.id, e.pNombre,   e.pApellido, e.direccion from estudiante as e 
            inner join curso as c on c.grado_id = e.grado_id JOIN cicloalumno as ca on ca.estudiante_id = e.id and ca.ciclo_id='.$cicloEscolar->id);
             return view('notas.notaAlumnoCreate')->with('carreras', $carreras)
             ->with('estudiantes', $estudiantes)
             ->with('unidades', $unidades)
             ->with('cursos', $cursos);

        }
        
    }

    public function getCursosAlumno(Request $request){
        if($request->ajax())
        {
            $usuarioId = auth()->id();
            $estudianteId=$request->id;
            $usuario = User::whereId($usuarioId)->get();
            $cicloEscolar = Ciclo::select('id', 'ciclo','estado_id')->orderBy('id', 'DESC')->first();
            foreach ($usuario as $tipo){
    
                $tipoUsuarioId= $tipo->tipousuarios_id;
            }
            if ($tipoUsuarioId!=1)
            {
          
            $cursos = DB::select('select c.id, a.id as "asignacion_id",  c.nombre from estudiante as e
            join asignacion as a on e.id = a.estudiante_id join curso as c on c.id = a.curso_id where e.id = '.$estudianteId.' and c.usuario_id ='.auth()->id());
    
            return Response($cursos);
            } else 
            {
                $cursos = DB::select('select c.id, a.id as "asignacion_id",  c.nombre from estudiante as e
                join asignacion as a on e.id = a.estudiante_id join curso as c on c.id = a.curso_id where e.id = '.$estudianteId);
                return Response($cursos);
                
            }

        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notasCursoStore(Request $request) //almacena todas las notas de todos los estudiantes de un curso en especifico
    {
        // $data1 = $request->all(); //opcion valida tambien para la recepcion de la data de jquery        
       $data = json_decode($request->arrayDatos);
        //    foreach ($data as $objData){
        //     $notaCurso = $objData->calificacion;
        //     // $alumno = $objData->idAlumno; // viene dentro del array para futuros usos
        //     $unidad = $objData->unidad;
        //     $curso = $objData->curso;
        //     $asignacion = $objData->idAsignacion;
            
        //     $nuevaNota = new Nota;
        //     $nuevaNota->nota = $notaCurso;
        //     $nuevaNota->unidad_id = $unidad;
        //     $nuevaNota->asignacion_id = $asignacion;
        //     $nuevaNota->save();

            
            
        //    } 

        
       return response($data);

       
    }

    public function notaAlumnoStore(Request $request) //almacena la nota de los cursos de un estudiante especifico
    {
        $curso = $request->cursos;
        $nota = $request->notaUnidad;
        $unidad = $request->unidad;
        $estudianteId = $request->estudianteId;
        $asignacion = DB::select('select a.id from asignacion as a where curso_id = '.$curso.' and estudiante_id =  '.$estudianteId);
        foreach ($asignacion as $a){

            $asignacionId = $a->id;
            
        }

        $nuevaNota = new Nota;
        $nuevaNota->nota = $nota;
        $nuevaNota->unidad_id = $unidad;
        $nuevaNota->asignacion_id = $asignacionId;

        $nuevaNota->save();


        return redirect(route('panelBusquedaCurso'))->with('status', 'Nota registrada exitosamente');



    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
