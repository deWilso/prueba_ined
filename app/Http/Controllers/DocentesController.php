<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tipousuario;


class DocentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docentes = User::orderBy('id', 'ASC')->paginate(10);
         $roles = Tipousuario::all();
        return view('docente.lista')->with('docentes', $docentes)
                                    ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
     return view('docente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // $rules = [ 
        //     'dpi'=>'required|unique:users',
        //     'email'=>'unique:users'
            
        //   ];
         
        //   $messages = [ 
        //     'dpi.unique'=>'El numero de identificacion ya existe.',
        //      'email.unique'=>'el correo electronico ya existe',
        //   ];
        //  $this->validate($request, $rules, $messages); 
        $identificacion = $request->dpi; 
        $pNombre = $request->pNombre;
        $sNombre = $request->sNombre;
        $pApellido = $request->pApellido;
        $sApellido = $request->sApelido;
        $direccion = $request->direccion;
        $telefono = $request->telefono;
        $email = $request->email;
        $password = $request->pass;
        $rol = $request->tipo;

        $user= new User;
        $user->dpi = $identificacion;
        $user->pNombre = $pNombre;
        $user->sNombre = $sNombre;
        $user->pApellido = $pApellido;
        $user->sApellido = $sApellido;
        $user->direccion = $direccion;
        $user->telefono = $telefono;
        $user->email = $email;
        
        $user->password = bcrypt($password);
        $user->tipousuarios_id=$rol;
        $user->save();
        return redirect(route('docentesList'))->with('status', 'usuario '.$user->pNombre.' creado con exito');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if($request->ajax())
                {
                    $id=$request->id;
                   
                    $user = User::whereId($id)->get();
                    
                    return Response($user);
                }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                $docente = User::whereId($id)->firstOrFail();
                $docente->tipousuarios_id = $request->get('tipo');
                $docente->dpi = $request->get('dpi');
                $docente->pNombre = $request->get('pNombre');
                $docente->sNombre = $request->get('sNombre');   
                $docente->pApellido = $request->get('pApellido');
                $docente->sApellido = $request->get('sApellido');
                $docente->direccion = $request->get('direccion');
                $docente->telefono = $request->get('telefono');
                $docente->email = $request->get('email');
                $docente->password = bcrypt($request->get('pass'));           
                $docente->save();
                return redirect(route('docentesList'))->with('status', 'Docente/Usuario '.$docente->pNombre.' Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::whereId($id)->firstOrFail();
        $user->delete();
        return redirect(route('docentesList'))->with('status', 'Docente/Usuario '.$user->pNombre.' eliminado con exito');
    }
}
