<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    //
    protected $table = 'nota';
    protected $fillable = [
        'id','valor', 'asignacion_id','unidad_id'

    ];
    public function asignacion(){
        return $this->belongsTo('App\Asignacion');
    }
    public function unidades (){
        return $this->belongsTo('App\Unidad');
    }
}
