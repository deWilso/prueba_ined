<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    //
    protected $table = 'curso';
    protected $fillable = [
        'id', 'codigo','nombre', 'usuario_id', 'grado_id'
    ];

    public function docentes (){
        return $this->belongsTo('App\User', 'usuario_id' );
    }
    public function grados(){
        return $this->belongsTo('App\Grado', 'grado_id');
    }
    public function asignaciones(){
        return $this->hasMany('App\Asignacion');
    }
}
