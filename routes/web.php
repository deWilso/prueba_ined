<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();



Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
//rutas ciclo
Route::get('cicloEscolar', 'CiclosController@index')->name('cicloEscolar');
Route::post('cerrarCicloEscolar/{id?}', 'CiclosController@cerrarCiclo')->name('cerrarCicloEscolar');
Route::post('cicloStore', 'CiclosController@store')->name('cicloStore');
//Rutas docente
Route::get('/docentesList', 'DocentesController@index')->name('docentesList');
Route::get('/docenteEdit', 'DocentesController@edit')->name('docenteEdit');
Route::post('/docenteDestroy/{id?}', 'DocentesController@destroy')->name('docenteDestroy');
Route::post('/docenteStore', 'DocentesController@store')->name('docenteStore');
Route::post('/docenteUpdate/{id?}', 'DocentesController@update')->name('docenteUpdate');

//Rutas Carreras
Route::get('/carreraList', 'CarrerasController@index')->name('carreraList');
Route::get('/carreraEdit', 'CarrerasController@edit')->name('carreraEdit');
Route::post('/carreraDestroy/{id?}', 'CarrerasController@destroy')->name('carreraDestroy');
Route::post('/carreraStore', 'CarrerasController@store')->name('carreraStore');
Route::post('/carreraUpdate/{id?}', 'CarrerasController@update')->name('carreraUpdate');


//rutas grados
Route::get('/gradoList', 'GradosController@index')->name('gradoList');
Route::get('/gradoEdit', 'GradosController@edit')->name('gradoEdit');
Route::post('/gradoDestroy/{id?}', 'GradosController@destroy')->name('gradoDestroy');
Route::post('/gradoStore', 'GradosController@store')->name('gradoStore');
Route::post('/gradoUpdate/{id?}', 'GradosController@update')->name('gradoUpdate');
Route::get('/getGrados', 'GradosController@getGrados')->name('getGrados');

//Rutas estudiante
Route::get('/estudianteList', 'EstudiantesController@index')->name('estudianteList');
Route::get('/estudianteEdit', 'EstudiantesController@edit')->name('estudianteEdit');
Route::get('/estudianteCreate', 'EstudiantesController@create')->name('estudianteCreate');
Route::post('/estudianteDestroy/{id?}', 'EstudiantesController@destroy')->name('estudianteDestroy');
Route::post('/estudianteStore', 'EstudiantesController@store')->name('estudianteStore');
Route::post('/estudianteUpdate/{id?}', 'EstudiantesController@update')->name('estudianteUpdate');

Route::post('/notaAlumnoStore', 'NotasController@lala')->name('notaAlumnoStore');
//rutas cursos
Route::get('/cursoList', 'CursosController@index')->name('cursoList');
Route::get('/cursoEdit', 'CursosController@edit')->name('cursoEdit');
Route::post('/cursoDestroy/{id?}', 'CursosController@destroy')->name('cursoDestroy');
Route::post('/cursoStore', 'CursosController@store')->name('cursoStore');
Route::post('/cursoUpdate/{id?}', 'CursosController@update')->name('cursoUpdate');

//rutas notas 

Route::get('/panelBusquedaCurso', 'NotasController@panelBusquedaCurso')->name('panelBusquedaCurso');
Route::post('/searchCurso', 'NotasController@searchCurso')->name('searchCurso');

// Route::get('/notaGrupo', 'NotasController@index')->name('notaGrupo');
Route::post('/notaAlumnoStore', 'NotasController@notaAlumnoStore')->name('notaAlumnoStore');
Route::post('/notaCursoStore', 'NotasController@notasCursoStore')->name('notaCursoStore');


Route::get('/panelBusquedaAlumno', 'NotasController@panelBusquedaAlumno')->name('panelBusquedaAlumno');
Route::post('/searchAlumno', 'NotasController@searchAlumno')->name('searchAlumno');


Route::get('/getCursosAlumno/{id?}' , 'NotasController@getCursosAlumno')->name('getCursosAlumno');
Route::get('/getAlumnosCurso/{id?}', 'NotasController@getAlumnosCurso')->name('getAlumnosCurso');

//Reportes 
Route::post('/reporteCalificacionesDeGrado', 'ReportesController@reporteCalificacionesDeGrado')->name('reporteCalificacionesDeGrado');
Route::post('/reporteCalificacionesEstudiante', 'ReportesController@reporteCalificacionesEstudiante')->name('reporteCalificacionesEstudiante');
Route::get('/generaReporteGrado', 'ReportesController@generaReporteGrado')->name('generaReporteGrado');
Route::get('/generaReporteEstudiante', 'ReportesController@generaReporteEstudiante')->name('generaReporteEstudiante');
