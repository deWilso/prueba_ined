<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante', function (Blueprint $table) {
            $table->increments('id');
            $table->string('carnet', 50)->nullable();
            $table->string('pNombre', 30);
            $table->string('sNombre', 30)->nullable();
            $table->string('tNombre', 30)->nullable();
            $table->string('pApellido', 30);
            $table->string('sApellido', 30)->nullable();
            $table->string('aCasada', 30)->nullable();
            $table->string('direccion', 100);
            $table->string('telefono', 16)->nullable();
            $table->date('fechaNacimiento');
            $table->string('estabAnterior', 100);
            $table->string('dpi', 30)->nullable();
            $table->unsignedInteger('genero_id');
            $table->foreign('genero_id')->references('id')->on('genero');
            $table->unsignedInteger('grado_id');
            $table->foreign('grado_id')->references('id')->on('grado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiante');
    }
}
