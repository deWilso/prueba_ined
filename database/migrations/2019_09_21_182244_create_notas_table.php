<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota', function (Blueprint $table) {
            $table->increments('id');
            $table->double('nota', 8,2);
            $table->unsignedInteger('unidad_id');
            $table->foreign('unidad_id')->references('id')->on('unidad');
            $table->unsignedInteger('asignacion_id');
            $table->foreign('asignacion_id')->references('id')->on('asignacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota');
    }
}
