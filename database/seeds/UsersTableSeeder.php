<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //tipo usuarios
        DB::table('tipousuarios')->insert([
            'tipo' => 'Administrador',
        ]);

        DB::table('tipousuarios')->insert([
            'tipo' => 'Docente',
        ]);

         //usuarios
         DB::table('users')->insert([
             'dpi'=>'2250303571808',
            'pNombre' => 'Cornelio',
            'pApellido'=>'leal',
            'direccion'=>'Santa Amelia',
            'telefono'=>'77231810',
            'email' => 'cornelio@gmail.com',
            'password' => bcrypt('12345678'),
            'tipousuarios_id' => '2',
        ]);

         DB::table('users')->insert([
            'dpi'=>'1310456894512',
            'pNombre' => 'Uriel',
            'pApellido'=>'Garcia',
            'direccion'=>'Santa Amelia',
            'telefono'=>'53104278',
            'email' => 'uriel@gmail.com',
            'password' => bcrypt('12345678'),
            'tipousuarios_id' => '1',
        ]);
        DB::table('users')->insert([
            'dpi'=>'2251303281416',
            'pNombre' => 'Juan',
            'pApellido'=>'de la Cruz',
            'direccion'=>'Santa Amelia',
            'telefono'=>'30727471',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('12345678'),
            'tipousuarios_id' => '1',
        ]);
    }
}
