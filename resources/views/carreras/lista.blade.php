@extends('layouts.app')
@section('title')
    Carreras 
@stop

@section('description')
    Gestion de Carreras
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Carreras </a></li>
        <li class="active">Carreras</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Listado de Carreras</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <a type="button" id="agregarCarrera"href="#" class="btn btn-block btn-primary">
                        <span class="glyphicon glyphicon-plus"> Agregar Carrera</span>
                    </a>
                </div>
                
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!-- verificamos que la tabla no este vacia-->
            @if ($carreras->isEmpty())
                <div>No hay registro de carreras</div>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre de la Carrera</th>
                            <th>Jornada</th>
                            <th>Accion</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($carreras as $carrera)
                            <tr>
                                 <td>{!! $carrera->nombre !!}</td> 
                                 <td>{!! $carrera->jornadas->jornada  !!}</td>     
                                
                                <td>
                                <!-- <form method="post" action="{!! action('CarrerasController@destroy', $carrera->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                    <button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                    </div>                
                                </form> -->

                                <a href="#" id="btnEditarCarrera" data-value="{!! $carrera->id !!}" class="btnEditarCarrera"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $carreras->render()!!}
            @endif
            
        </div>
    </div>
    @include ("carreras.carreraModal");


<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
<script>
$(document).ready(function() {
$('div.alert').delay(5000).slideUp(300);
$('.btnEditarCarrera').click(function() 
	{
	   
	   		var id = $(this).data("value"); 

			console.log(id);
			var form = $('#formulario');
			
			var urlTemp="{{route('carreraUpdate', ':carrera_id')}}";
			var url = urlTemp.replace(':carrera_id', id);
			document.forms["formulario"].action=url;		
			alert (url);
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('carreraEdit')}}',
				data:{'id':id},
				success:function(data)
				{
				console.log(data);
				$('#jornada option[value="'+data[0].jornada_id+'"]').prop('selected', true);
				$("#nombreCarrera").val(data[0].nombre);
				
				}
			});
			
	  		$('#nombreModal').text("Editar Carrera");
			$("#carrera").modal({backdrop: 'static', keyboard: false}); 
            // $("#carrera").modal();
			$.getScript("js/validaCarrera.js"); 

			
	 });

$('#agregarCarrera').click(function() 
{		
    document.forms["formulario"].action="{{route ('carreraStore')}}"; 
   
    $('#nombreModal').text("Nueva Carrera");
    $("#carrera").modal({backdrop: 'static', keyboard: false});
            
    $.getScript("js/validaCarrera.js"); 
});

});
</script>
 @stop