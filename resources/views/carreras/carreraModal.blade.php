<div class="modal" fade id="carrera">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
        
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal"  modal-backdrop>
                <span>&times;</span>
             </button>
             <h1 id="nombreModal"></h1>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="modal-body">
            <form class="form-horizontal" name="formulario" id="formulario"method="POST" action="#">
            {{csrf_field()}}
            @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
            <div class="box-body">
                <div class="form-group">
                    <label for="dpi" class="col-sm-2 control-label">Nombre de la Carrera</label>

                    <div class="col-sm-6">
                        <input type="text" name ="nombreCarrera" class="form-control" id="nombreCarrera" placeholder="Bachillerato ..." value="">
                    </div>    
                </div>
                <div class="form-group">
                                            
                        <label for="jornada" id="lbljornada" class="col-sm-2 control-label">Jornada</label>
                        <div class="col-sm-3"> 
                              <select class="form-control" name="jornada" id="jornada">
                                <option value="0">--Seleccione la Jornada--   </option>
                                @foreach($jornadas as $jornada)
                                <option value="{{$jornada->id}}">{{$jornada->jornada}}</option>
                                @endforeach
                              </select>
                        </div>
                </div>
            </div>                 
            
            <div class="modal-footer" style="margin-top: 15px; text-align: center">
              <input type="button" id="cancelar" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
              <input type="submit" id="guardar" class="btn btn-primary" value="Guardar">
            </div>
        </form>
        </div>    
    </div>

</div>