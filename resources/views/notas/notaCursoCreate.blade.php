@extends('layouts.app')

@section('title')
    Ingreso de notas
@stop

@section('description')
    
@stop
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Buscar estudiante</a></li>
        <li class="active">Agregar informacion para la busqueda</li>
    </ol>
@stop

@section('content')
<div class="col-sm-12"   >
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Datos del estudiante</h3>
            </div>
        
            <!-- /.box-header -->
            <!-- form start -->

                <div class="alert alert-success" id="alerta-success">
                   
                </div>
           
            <form name="formNotasCurso" id="formNotasCurso" class="form-horizontal" method="POST" action="#">
            {{csrf_field()}}
             <!-- recorrido de errores en el formulario -->
              @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif              
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2" for="">Curso</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="curso" id="curso">
                       <option value="0">--Seleccione el curso--</option>
                      @foreach($cursos as $curso)
                       <option value="{{$curso->id}}">{{$curso->nombre}}</option>
                      @endforeach
                    </select>
                  </div>

                  <label class="col-sm-2" for="">Unidad</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="unidad" id="unidad">
                       <option value="0">--Seleccione la Unidad a calificar--</option>
                      @foreach($unidades as $unidad)
                       <option value="{{$unidad->id}}">{{$unidad->unidad}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <!-- ----- -->
              <table class="table">
                    <thead>
                        <tr>
                            <th>Primer Nombre</th>
                            <th>Segundo Apellido</th>
                            <th>Nota</th>
                        </tr>
                    </thead>
                    <tbody id="cuerpoTabla">
                       
                    </tbody>
                </table>

              <!-- --------- -->
              <div class="box-footer" style="margin-top: 15px; text-align: center">
                    <a href="#" class="btn btn-danger">Cancelar</a>                  
                    <button type="button" id="btnGuardarNotas" class="btn btn-info"> Guardar </button>                
              </div>    


            </form>
            
        </div>
        </div>
<script>

$(document).ready( function ()
 {
    // $('div.alert').delay(5000).slideUp(300);
    $('div.alert').hide();
  
    $('#curso').on('change', function() 
	{
      $("#cuerpoTabla tr").remove();  
	  	var id = $(this).val();
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getAlumnosCurso')}}',
				data:{'id':id},
				success:function(data)
				{
          console.log(data);
          for (var i=0; i<data.length;i++)
          {
          
            $('#cuerpoTabla')
            .append('<tr><td>' + data[i].pNombre + '</td><td>' + data[i].pApellido + '</td><td><input type="number" id="'+i+'" name="'+data[i].id+'" data-asignacion="'+data[i].asignacion+'"></td></tr>');
            
          }                                    
				}
	  	});
				
	 });

   $('#btnGuardarNotas').on('click', function (){
       
        var datos= [];
        var idUnidad = $('#unidad').val();
        var idCurso = $('#curso').val();
        $("#cuerpoTabla").find(':input').each(function() {
        

          var alumno= $(this).attr("name");
          var idInput= $(this).attr("id");
          var asignacion = $(this).attr("data-asignacion");
          var nota = $(this).val();

          datos.push({idAlumno: alumno, calificacion: nota, unidad: idUnidad, curso: idCurso, idAsignacion: asignacion });
           //  alert ("alumnoID: "+alumno+" , idTxt:"+idInput+ ", nota:"+nota);
     
        });

        // console.log(datos);
        var arrayDatos = JSON.stringify(datos);
        var request;  
        request = $.ajax
        ({
          method: "POST",
          dataType: "json",
          url : '{{URL::to('notaCursoStore')}}',
          data: {'arrayDatos': arrayDatos},    
          success: function(html)
            {
              console.log(html);
              var mensaje = "Notas actualizadas con exito";
             
              // $("#alerta-success").on('closed.bs.alert', function(){
              // alert("Alert message box has been closed.");
              //  });
              //  $(document).ajaxStop(function(){ window.location.reload(); }); 
              
                 $('#alerta-success').append("<strong>Exito!</strong> Calificaciones ingresadas .");
                 $('div.alert').show();
                 $('div.alert').delay(5000).slideUp(300);
                // setTimeout(function () { document.location.reload(true); }, 5000);
              
             }
        });
        

   });




           
}); 

</script>
        
@stop