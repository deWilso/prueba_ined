<div class="modal" fade id="agregarNotaEstudiante">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
        
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">
                <span>&times;</span>
             </button>
             <h1 id="nombreModal"></h1>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="modal-body">
            <form class="form-horizontal" name="formulario" id="formulario" method="post" action="{{ route ('notaAlumnoStore') }}">
            {{csrf_field()}}
            @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
            <div class="box-body">
                
                <div class="form-group">

                                            
                        <label for="" class="col-sm-2 control-label">Curso</label>
                        <div class="col-sm-4"> 
                              <select class="form-control" name="cursos" id="cursos">
                               <option value="0">--Seleccione el Curso--</option>
                              </select>
                        </div>
                        <label for="unidad" id="lblUnidad" class="col-sm-2 control-label">Unidad</label>
                        <div class="col-sm-4"> 
                              <select class="form-control" name="unidad" id="unidad">
                                <option value="0">--Seleccione la unidad-- </option>
                                @foreach($unidades as $unidad)
                                 <option value="{{$unidad->id}}">{{$unidad->unidad}}</option>
                                @endforeach
                              </select>
                        </div>
                </div> <br><br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Ingrese nota final de unidad</label>

                    <div class="col-sm-6">
                        <input type="text" name ="notaUnidad" class="form-control" id="notaUnidad" value="">
                        <input type="hidden" name ="estudianteId" class="form-control" id="estudianteId" value="">

                    </div>    
                </div>
            </div>                 
            
            <div class="modal-footer" style="margin-top: 15px; text-align: center">
              <input type="button" id="cancelar" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
              <input type="submit" id="guardar" class="btn btn-primary" value="Guardar">
            </div>
        </form>
        </div>    
    </div>

</div>