@extends('layouts.app')

@section('title')
    Calificaciones
@stop

@section('description')
    Registro  de calificaciones por estudiante determinado
@stop
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route ('estudianteList')}}"><i class="fa fa-dashboard"></i> Buscar estudiante</a></li>
        <li class="active">Agregar informacion para la busqueda</li>
    </ol>
@stop

@section('content')
        <div class="col-sm-12"   >
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Lista de Alumnos</h3>
            </div>
        
            <!-- /.box-header -->
            <!-- form start -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

              @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
              @if (!$estudiantes)
                <div>No hay registro de estudiantes</div>
            @else
                  <div class="box-body">
                 
                     <div class="col-sm-12">
                     <table class="display" id="tabla_alumnos">
                    <thead>
                        <tr>
                            <th>Carnet</th>
                            <th>Primer Nombre</th>
                            <th>Primer Apellido</th>
                            <th>Direccion</th>
                            <th>Accion</th>
                          </tr>
                    </thead>
                    <tbody>
                        @foreach($estudiantes as $estudiante)
                            <tr>
                                 <td>{!! $estudiante->carnet!!}</td>      
                                <td>{!! $estudiante->pNombre !!}</td>
                                
                                <td>{!! $estudiante->pApellido !!}</td> 
                                <td>{!! $estudiante->direccion !!}</td>
                               

                                <td>
                                <a href="#" id="agregarNota" data-value="{!! $estudiante->id !!}" class="agregarNota"><span class='glyphicon glyphicon-edit'>Agregar Nota</span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                @endif
                 </div>
            </div>
        </div>
        </div>
        @include('notas.agregarNotaEstudianteModal')
<script>

$(document).ready( function () {
  $('div.alert').delay(5000).slideUp(300);
    $('#tabla_alumnos').DataTable(); 
   
    $('.agregarNota').click(function() 
	{
	   
	   		var id = $(this).data("value"); 
         $("#estudianteId").val(id);
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getCursosAlumno')}}',
				data:{'id':id},
				success:function(data)
				{
          console.log(data);

          for (var i=0; i<data.length;i++)
          {
          
          $("#cursos").append("<option  data-asignacion = '"+data[0].asignacion_id+"'value='" + data[i].id +"'>"+data[i].nombre+"</option>");
           
          }                                    
				}
		});
			
      $('#nombreModal').text("Agregar Nota"); 
      $("#agregarNotaEstudiante").modal({backdrop: 'static', keyboard: false});
			$.getScript("js/validaNotaEstudiante.js"); 

			
	    });

       //funcion para limpiar modal despues de cierre
       $('#agregarNotaEstudiante').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
            $("#cursos").empty();
            
        });
        
           
}); 

</script>
        
@stop