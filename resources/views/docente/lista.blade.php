@extends('layouts.app')
@section('title')
    Gestion de Docentes
@stop

@section('description')
    Docentes
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Docentes </a></li>
        <li class="active">Gestion de docentes</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Docentes</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <a type="button" id="agregar_docente"href="#" class="btn btn-block btn-primary">
                        <span class="glyphicon glyphicon-plus"> Agregar Docente </span>
                    </a>
                </div>
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!-- verificamos que la tabla no este vacia-->
            @if ($docentes->isEmpty())
                <div>No hay registro de docentes</div>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>DPI</th>
                            <th>Primer Nombre</th>
                            
                            <th>Primer Apellido</th>
                           
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th>Email</th>
                            
                            <th>Rol</th>
                            <th>Accion</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($docentes as $docente)
                            <tr>
                                 <td>{!! $docente->dpi       !!}</td>      
                                <td>{!! $docente->pNombre !!}</td>
                                
                                <td>{!! $docente->pApellido !!}</td>
                                
                                <td>{!! $docente->direccion !!}</td>
                                <td>{!! $docente->telefono !!}</td>
                                <td>{!! $docente->email !!}</td>
                            
                                <td>{!! $docente->tipousuario->tipo!!}</td>
                                <td>
                                <!-- <form method="post" action="{!! action('DocentesController@destroy', $docente->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                    <button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                    </div>                
                                </form> -->

                                <a href="#" id="btnEditarDocente" data-value="{!! $docente->id !!}" class="btnEditarDocente"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $docentes->render()!!}
            @endif
            
        </div>
    </div>
    @include ("docente.docenteModal")





<script>
$(document).ready(function() {
$('div.alert').delay(5000).slideUp(300);
$('.btnEditarDocente').click(function() 
	{
	   
	   		var id = $(this).data("value"); 

			console.log(id);
			var form = $('#formulario');
			
			var urlTemp="{{route('docenteUpdate', ':user_id')}}";
			var url = urlTemp.replace(':user_id', id);
			document.forms["formulario"].action=url;		
			alert (url);
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('docenteEdit')}}',
				data:{'id':id},
				success:function(data)
				{
				console.log(data);
				$('#tipo option[value="'+data[0].tipousuarios_id+'"]').prop('selected', true);
				$("#dpi").val(data[0].dpi);
				$("#pNombre").val(data[0].pNombre);
                $("#sNombre").val(data[0].sNombre);
                $("#pApellido").val(data[0].pApellido);
                $("#sApellido").val(data[0].sApellido);
                $("#direccion").val(data[0].direccion);
                $("#telefono").val(data[0].telefono);
                $("#email").val(data[0].email);
                $("#pass").val(data[0].pasword);
                
                
				}
			});
			
	  		$('#nombreModal').text("Editar Docente");
			$("#docente").modal({backdrop: 'static', keyboard: false});
			$.getScript("js/validaDocente.js"); 

			
	 });

$('#agregar_docente').click(function() 
{		
    document.forms["formulario"].action="{{route ('docenteStore')}}"; 
   
    $('#nombreModal').text("Nuevo Docente");
    $("#docente").modal({backdrop: 'static', keyboard: false});
            
    $.getScript("js/validaDocente.js"); 
});

});
</script> 

        
@stop
