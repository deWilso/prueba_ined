<div class="modal" fade id="docente">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
        
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">
                <span>&times;</span>
             </button>
             <h1 id="nombreModal"></h1>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="modal-body">
            <form class="form-horizontal" name="formulario" id="formulario"method="POST" action="#">
            {{csrf_field()}}
            @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
            <div class="box-body">
                <div class="form-group">
                                            
                        <label for="tipo" id="lblrol" class="col-sm-2 control-label">Rol *</label>
                        <div class="col-sm-3"> 
                              <select class="form-control" name="tipo" id="tipo">
                                <option value="0">--Seleccione Rol-- </option>
                                @foreach($roles as $rol)
                                <option value="{{$rol->id}}">{{$rol->tipo}}</option>
                                @endforeach
                              </select>
                        </div>
                </div>
                <div class="form-group">
                  <label for="dpi" class="col-sm-2 control-label">DPI *</label>

                  <div class="col-sm-6">
                    <input type="number" name ="dpi" class="form-control" id="dpi"  value="">
                  </div>    
                </div>
                
                <div class="form-group">
                  <label for="pNombre" class="col-sm-2 control-label">Primer Nombre *</label>

                  <div class="col-sm-6">
                    <input type="text" name ="pNombre" class="form-control" id="pNombre" placeholder="Primer Nombre" value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="sNombre" class="col-sm-2 control-label">Segundo Nombre</label>

                  <div class="col-sm-6">
                    <input type="text" name ="sNombre" class="form-control" id="sNombre" placeholder="Segundo Nombre" value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="pApellido" class="col-sm-2 control-label">Primer Apellido *</label>

                  <div class="col-sm-6">
                    <input type="text" name ="pApellido" class="form-control" id="pApellido" placeholder="Primer Apellido" value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="sApellido" class="col-sm-2 control-label">Segundo Apellido</label>

                  <div class="col-sm-6">
                    <input type="text" name ="sApellido" class="form-control" id="sApellido" placeholder="Segundo Apellido" value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="direccion" class="col-sm-2 control-label">Direccion *</label>

                  <div class="col-sm-6">
                    <input type="text" name ="direccion" class="form-control" id="direccion" placeholder="Direccion" value="">
                  </div>    
                </div>
                
                <div class="form-group">
                  <label for="telefono" class="col-sm-2 control-label">Telefono *</label>

                  <div class="col-sm-6">
                    <input type="number" name ="telefono" class="form-control" id="telefono" placeholder="77231010" value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email *</label>

                  <div class="col-sm-6">
                    <input type="text" name ="email" class="form-control" id="email" placeholder="juan@gmail.com" value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="pass" class="col-sm-2 control-label">Contraseña *</label>

                  <div class="col-sm-6">
                    <input type="password" name ="pass" class="form-control" id="pass"  value="">
                  </div>    
                </div>
                <div class="form-group">
                  <label for="pass" class="col-sm-2 control-label">Repetir contraseña *</label>

                  <div class="col-sm-6">
                    <input type="password" name ="pass2" class="form-control" id="pass2" value="">
                  </div>    
                </div>
                             
                
                 
            
            <div class="modal-footer" style="margin-top: 15px; text-align: center">
              <input type="button" id="cancelar" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
              <input type="submit" id="guardar" class="btn btn-primary" value="Guardar">
            </div>
        </form>
        </div>    
    </div>

</div>