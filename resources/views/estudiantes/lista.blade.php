@extends('layouts.app')
@section('title')
    Estudiantes
@stop

@section('description')
    Gestion de estudiantes
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Estudiantes</a></li>
        <li class="active">Estudiantes</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Estudiantes</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <a type="button" id="agregarGrado"href="{{route('estudianteCreate')}}" class="btn btn-block btn-primary">
                        <span class="glyphicon glyphicon-plus"> Nuevo Estudiante</span>
                    </a>
                </div>
                
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!-- verificamos que la tabla no este vacia-->
            @if ($estudiantes->isEmpty())
                <div>No hay registro de estudiantes</div>
            @else
            <table class="table">
                    <thead>
                        <tr>
                            <th>Carnet</th>
                            <th>Primer Nombre</th>
                            <th>Primer Apellido</th>
                            <th>Fecha Nacimiento</th>
                            <th>Direccion</th>
                            <th>Telefono</th>
                            <th>Ciclo Escolar</th>
                            <th>Accion</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($estudiantes as $estudiante)
                            <tr>
                                 <td>{!! $estudiante->carnet!!}</td>      
                                <td>{!! $estudiante->pNombre !!}</td>
                                
                                <td>{!! $estudiante->pApellido !!}</td>
                                <td>{!! $estudiante->fechaNacimiento !!}</td>
                                <td>{!! $estudiante->direccion !!}</td>
                                <td>{!! $estudiante->telefono !!}</td>                          
                                <td>
                                <form method="post" action="{!! action('EstudiantesController@destroy', $estudiante->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                    <button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                    </div>                
                                </form>

                                <a href="#" id="btnEditarEstudiante" data-value="{!! $estudiante->id !!}" class="btnEditarEstudiante"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $estudiantes->render()!!}
            @endif
            
        </div>
    </div>



 @stop