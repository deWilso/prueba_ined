@extends('layouts.app')

@section('title')
    Estudiantes
@stop

@section('description')
    Registro y lista de estudiantes
@stop
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{route ('estudianteList')}}"><i class="fa fa-dashboard"></i> Registro de Estudiantes</a></li>
        <li class="active">Estudiantes</li>
    </ol>
@stop

@section('content')
        <div class="col-sm-12"   >
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Datos del estudiante</h3>
            </div>
        
            <!-- /.box-header -->
            <!-- form start -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

 

            
            <form name="formularioEst" id="formularioEst" class="form-horizontal" method="POST" action="{{route('estudianteStore')}}">
            {{csrf_field()}}
             <!-- recorrido de errores del formulario -->
              @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
              
              <div class="box-body">
                <div class="form-group">
                  <label for="lblCarnet" class="col-sm-2 control-label">Carnet</label>

                  <div class="col-sm-4">
                    <input type="text" name ="carnet" class="form-control" id="carnet" placeholder="carnet" value="{{old('carnet')}}">
                    <span class="text-danger">{{ $errors->first('carnet') }}</span>
                  </div>
                  <label for="lbldpi" class="col-sm-2 control-label">Identificacion</label>
                  <div class="col-sm-4">
                  
                    <input type="number" name ="dpiEst" class="form-control" id="dpiEst" placeholder="dpi" value="{{old('dpiEst')}}">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="lblpNombreEst" class="col-sm-2 control-label">Primer Nombre</label>

                  <div class="col-sm-4">
                    <input type="text" name ="pNombreEst" class="form-control" id="pNombreEst" placeholder="Primer Nombre" value="{{old('pNombreEst')}}" >
                  </div>
                  <label for="lblsNombreEst" class="col-sm-2 control-label">Segundo Nombre</label>

                  <div class="col-sm-4">
                    <input type="text" name ="sNombreEst"class="form-control" id="sNombreEst" placeholder="Segundo Nombre" value="{{old('sNombreEst')}}">
                  </div>

                </div>
                <div class="form-group">
                  <label for="lbltNombreEst" class="col-sm-2 control-label">Tercer Nombre</label>
                  <div class="col-sm-4">
                    <input type="text" name ="tNombreEst" class="form-control" id="tNombreEst" placeholder="Tercer Nombre" value="{{old('tNombreEst')}}">
                  </div>
                  <label for="lblpApellidoEst" class="col-sm-2 control-label">Primer Apellido </label>
                  <div class="col-sm-4">
                      <input type="text" name="pApellidoEst" class="form-control" id="pApellidoEst" placeholder="Primer Apellido" value="{{old('pNombreEst')}}">
                  </div>
                </div>





                <div class="form-group">
                  <label for="lblsApellidoEst" class="col-sm-2 control-label">Segundo Apellido </label>

                  <div class="col-sm-4">
                        <input type="text" name="sApellidoEst" class="form-control" id="sApellidoEst" placeholder="Segundo Apellido" value="{{old('sApellidoEst')}}">        
                  </div>
                  <label for="lblaCasada" class="col-sm-2 control-label">Apellido de Casada</label>

                  <div class="col-sm-4">
                      <input type="text" name="aCasada" class="form-control" id="aCasada" placeholder="Apellido de Casada" value="{{old('aCasada')}}">  
                  </div>
                </div>



              
                <div class="form-group">
                  <label for="lbldireccionEst" class="col-sm-2 control-label">Direccion</label>

                  <div class="col-sm-4">
                    <input type="text" name ="direccionEst" class="form-control" id="direccionEst" placeholder="Direccion" value="{{old('direccionEst')}}"> 
                  </div>
                  <label for="lbltelEstudiante" class="col-sm-2 control-label">Telefono</label>

                  <div class="col-sm-4">
                    <input type="number" name ="telEstudiante" class="form-control" id="telEstudiante" placeholder="Telefono" value="{{old('telEstudiante')}}"> 
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="lblFecha" class="col-sm-2 control-label">Fecha de Nacimiento</label>

                  <div class="col-sm-4">
                    <input type="date" name ="fechaNac" class="form-control" id="fechaNac" placeholder=""value="{{old('fechaNac')}}"> 
                  </div>
                  <label for="genero" class="col-sm-2 control-label">Genero</label>

                  <div class="col-sm-4">
                  <select class="form-control" name="genero" id="genero">
                        <option>--Seleccione Genero--</option> 
                            @foreach($generos as $genero)
                        <option value="{{$genero->id}}">{{$genero->genero}}</option>
                            @endforeach
                   </select>
                  </div>
                  </div>
                
                <div class="form-group">
                <label for="" class="col-sm-2 control-label">Carrera</label>
                  <div class="col-sm-4">
                   
                    <select class="form-control" name="carrera" id="carrera">
                          <option>--Seleccione Carrera--</option> 
                              @foreach($carreras as $carrera)
                          <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                              @endforeach
                    </select>
                  </div>
                    <label for="" class="col-sm-2 control-label">Grado</label>

                    <div class="col-sm-4">                    
                      <select class="form-control" name="grado" id="grado">                         
                    
                      </select>
                   </div>
                  </div>          
            
                  <div class="form-group">
                  <label for="estAnterior" class="col-sm-2 control-label">Establecimiento Anterior</label>

                  <div class="col-sm-10">
                    <input type="text" name ="estabAnterior" class="form-control" id="estabAnterior" placeholder="Establecimiento Anterior" value="{{old('estabAnterior')}}"> 
                  </div>
                  
                </div>
              <!-- /.box-body -->
              <div class="box-footer" style="margin-top: 15px; text-align: center">
              <a href="{{route('estudianteList')}}" class="btn btn-danger">Cancelar</a>
                <!-- <input type="submit" id="btnGuardarEst" class="btn btn-info"value="Guardar"> -->
                <button type="submit" id="btnGuardarEst" class="btn btn-info"> Guardar </button>
                
              </div>
              <!-- /.box-footer -->
            </form>
            
        </div>
        </div>
        <script>

$(document).ready(function() {
  $('div.alert').delay(5000).slideUp(300);
  if ($("#formularioEst").length > 0) 
  {
    $("#formularioEst").validate(
  {
     
    rules: 
    {
      carnet:
       {
        required:true,

       },
       
       dpiEst: 
       {
           minlength: 13,
           maxlength:13,
           
       },
       pNombreEst: 
       {
           required: true,
           
       },
       
       
       pApellidoEst:
       {
           required: true,
           
       },
     
      
       direcionEst:
       {
           required: true,
           
       },
       
       fechaNac:
       {
        required:true,
       }, 
       
      telEstudiante:
       {
          
           minlength: 8,
           maxlength: 8,
           number:true,
       },
       
       genero:
       {
           selectcheck:true,
           required: true,
         
       },
       
       grado:
       {    
           selectcheck:true,
           required: true,
          
       },
       
       carrera:
       {    
           selectcheck:true,
           required: true,
          
       },
      
    },
    messages: 
    {
       
      carnet: {
           required: "Por favor Ingrese el carnet",
       },
      
       pNombreEst: {
           required: "Por favor ingrese un nombre",
       },
      
      
       pApellidoE: {
           required: "Por favor ingrese un apellido",
           
       
       },
       

       dpiEst: {
        required: "Por favor ingrese el numero de identificacion",
        minlength: "almenos 13 caracteres",
        },
 
        direccion: {
            required: "Por favor ingrese la direcccion",
            
        
        },
        
        telefono: {
            required: "Por favor ingrese el numero de telefono",
            minlength: "almenos 8 caracteres",
            
        
        },
        
        genero: {
            required: "Por favor seleccione el genero",
                  
        
        },
      
        grado: {
            required: "ppr favor seleccione el grado",
            
              
        },
        carrera: {
            required: "ppr favor seleccione la carrera",
            
              
        },
    
    
   
        
    },
    })
  }
// fin validacion

  $('#carrera').on('change', function() 
	{
	   
	   		var id = $(this).val();   
        console.log(id);
			
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getGrados')}}',
				data:{'id':id},
				success:function(data)
				{
          $("#grado").empty();
				console.log(data);
        for (var i=0; i<data.length;i++)
          {
          
          $("#grado").append("<option value='" + data[i].id +"'>"+data[i].nombre+"</option>");
                    
          }
                
                
				}
			});
			
   });
   
  //  $('#btnGuardarEst').click(function() 
  //   {		

  //      console.log("click");
  //      $.getScript("js/validaEstudiante.js");   
  //      document.forms["formularioEst"].action="{{route ('estudianteStore')}}";         
        
  //   });
 
});

</script> 

        
@stop