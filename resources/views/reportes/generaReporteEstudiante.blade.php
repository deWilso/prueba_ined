@extends('layouts.app')
@section('title')
    Reportes por Estudiante
@stop

@section('description')
   Generar reporte de calificaciones por estudiante
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Reportes </a></li>
        <li class="active">Reporte por Estudiante</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Generar reporte para</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <form method="post" action="{!! Route('generaReporte') !!}" class="form-horizontal">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="carrera" id="lblcarrera" class="col-sm-2 control-label">Carrera</label>
                            <div class="col-sm-6"> 
                                 <!-- <select class="form-control" name="carrera" id="carrera">
                                     <option value="0">--Seleccione la Carrera--   </option>
                                         @foreach($carreras as $carrera)
                                     <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                         @endforeach
                                 </select> -->
                             </div> 
                             <label for="carrera" id="lblGrado" class="col-sm-2 control-label">Grado</label>
                            <div class="col-sm-6"> 
                                 <select class="form-control" name="grado" id="grado">
                                     <option value="0">--Seleccione la Carrera--   </option>
                                    
                                 </select>
                             </div>                                   
                        </div>
                        <div>
                         <button type="submit" class="btn btn-primary">Generar Reporte de Grado</button>
                        </div>                
                    </form> 
                </div>
                
            </div>  
            
        </div>
</div>
@stop