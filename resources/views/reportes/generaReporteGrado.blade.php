@extends('layouts.app')
@section('title')
    Reportes 
@stop

@section('description')
   Generar reporte de calificaciones por grado
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Reportes </a></li>
        <li class="active">Reporte por Grado</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Reporte de Calificaciones</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-12">
                    <form method="post" action="{!! Route('reporteCalificacionesDeGrado') !!}" class="form-horizontal">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="carrera" id="lblcarrera" class="col-sm-2 control-label">Carrera</label>
                            <div class="col-sm-4"> 
                                 <select class="form-control" name="carrera" id="carrera">
                                     <option value="0">--Seleccione la Carrera--   </option>
                                         @foreach($carreras as $carrera)
                                     <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                         @endforeach
                                 </select>
                             </div> 
                             <label for="carrera" id="lblGrado" class="col-sm-2 control-label">Grado</label>
                            <div class="col-sm-4"> 
                                 <select class="form-control" name="grado" id="grado">
                                     <option value="0">--Seleccione el grado--   </option>
                                    
                                 </select>
                             </div>                                   
                        </div>
                        <div>
                         <button type="submit" class="btn btn-primary">Generar Reporte de Grado</button>
                        </div>                
                    </form> 
                </div>
                
            </div>  
            
        </div>
</div>
<script>
$(document).ready(function() {
 $('div.alert').delay(5000).slideUp(300);
$('#carrera').on('change', function() 
	{
	   
	   		var id = $(this).val();   
            console.log(id);
			
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getGrados')}}',
				data:{'id':id},
				success:function(data)
				{
                    $("#grado").empty();
                    console.log(data);
                    for (var i=0; i<data.length;i++)
                    {
                    
                    $("#grado").append("<option value='" + data[i].id +"'>"+data[i].nombre+"</option>");
                                
                    }
                
				}
			});
			
   });

});
</script> 
@stop