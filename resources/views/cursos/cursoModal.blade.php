<div class="modal" fade id="cursos">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
        
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">
                <span>&times;</span>
             </button>
             <h1 id="nombreModal"></h1>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="modal-body">
            <form class="form-horizontal" name="formulario" id="formulario"method="POST" action="#">
            {{csrf_field()}}
            @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
            <div class="box-body">
                <div class="form-group">
                                            
                        <label for="" id="" class="col-sm-2 control-label">Carrera</label>
                        <div class="col-sm-4"> 
                              <select class="form-control" name="carrera" id="carrera">
                                <option value="0">--Seleccione la Carrera--   </option>
                                @foreach($carreras as $carrera)
                                <option value="{{ $carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                              </select>
                        </div>
                        <label for="tipo" id="lblrol" class="col-sm-2 control-label">Grado</label>
                        <div class="col-sm-4"> 
                              <select class="form-control" name="grado" id="grado">
                                <option value="0">--Seleccione el Grado--   </option>
                                
                              </select>
                        </div>
                </div>

                <div class="form-group">
                  <label for="dpi" class="col-sm-2 control-label">Nombre del Curso</label>

                  <div class="col-sm-10">
                    <input type="text" name ="curso" class="form-control" id="curso" placeholder="Nombre del Curso" value="">
                  </div>    
                </div>

                <div class="form-group">
                <label for="tipo" id="lblrol" class="col-sm-2 control-label">Docente Designado</label>
                        <div class="col-sm-6"> 
                              <select class="form-control" name="docente" id="docente">
                                <option value="0">--Seleccione al docente responsable--   </option>
                                @foreach($docentes as $docente)
                                <option value="{{$docente->id}}">{{$docente->pNombre}} {{$docente->pApellido}}</option>
                                @endforeach
                              </select>
                        </div>
                </div>
  
            <div class="modal-footer" style="margin-top: 15px; text-align: center">
              <input type="button" id="cancelar" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
              <input type="submit" id="guardar" class="btn btn-primary" value="Guardar">
            </div>
        </form>
        </div>    
    </div>

</div>