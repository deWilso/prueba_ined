@extends('layouts.app')
@section('title')
    Gestion de Cursos 
@stop

@section('description')
    Lista de cusros, cracion de cursos y asignacion docente
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Cursos </a></li>
        <li class="active">Gestion de cursos</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Lista de cursos</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <a type="button" id="agregarCurso" href="#" class="btn btn-block btn-primary">
                        <span class="glyphicon glyphicon-plus"> Agregar Curso</span>
                    </a>
                </div>
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!-- verificamos que la tabla no este vacia-->
            @if ($cursos->isEmpty())
                <div>No hay registro de cursos</div>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre del Curso</th>
                            <th>Grado</th>
                            
                            <th>Docente</th>   
                            <th>Accion</th>                         
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cursos as $curso)
                            <tr>
                                 <td>{!! $curso->nombre !!}</td>      
                                <td>{!! $curso->grados->nombre !!}</td>                                
                                <td>{!! $curso->docentes->pNombre !!}</td>
                                
                            

                                <td>
                                <!-- <form method="post" action="{!! action('CursosController@destroy', $curso->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                    <button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                    </div>                
                                </form> -->

                                <a href="#" id="btnEditarCurso" data-value="{!! $curso->id !!}" class="btnEditarCurso"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $cursos->render()!!}
            @endif
            
        </div>
    </div>
    @include ("cursos.cursoModal")





<script>
$(document).ready(function() {
 $('div.alert').delay(5000).slideUp(300);

 $('.btnEditarCurso').click(function() 
	{
	   
	   		var id = $(this).data("value"); 

			console.log(id);
			var form = $('#formulario');
			
			var urlTemp="{{route('cursoUpdate', ':curso_id')}}";
			var url = urlTemp.replace(':curso_id', id);
			document.forms["formulario"].action=url;		
			alert (url);
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('cursoEdit')}}',
				data:{'id':id},
				success:function(data)
				{
				console.log(data);
				$('#grado option[value="'+data[0].grado_id+'"]').prop('selected', true);
                $("#curso").val(data[0].nombre);
                $('#docente option[value="'+data[0].usuario_id+'"]').prop('selected', true);
				}
			});
			
	  		$('#nombreModal').text("Editar Curso");
			$("#cursos").modal({backdrop: 'static', keyboard: false});
			$.getScript("js/validaCurso.js"); 

			
	 });


$('#agregarCurso').click(function() 
{		
    document.forms["formulario"].action="{{route ('cursoStore')}}"; 
   
    $('#nombreModal').text("Nuevo Curso");
    $("#cursos").modal({backdrop: 'static', keyboard: false});
            
    $.getScript("js/validaCurso.js"); 
});

$('#carrera').on('change', function() 
	{
	   
	   		var id = $(this).val();   
            console.log(id);
			
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('getGrados')}}',
				data:{'id':id},
				success:function(data)
				{
                    $("#grado").empty();
                    console.log(data);
                    for (var i=0; i<data.length;i++)
                    {
                    
                    $("#grado").append("<option value='" + data[i].id +"'>"+data[i].nombre+"</option>");
                                
                    }
                
				}
			});
			
   });

});
</script> 

        
@stop
