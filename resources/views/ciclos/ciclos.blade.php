@extends('layouts.app')
@section('title')
    Ciclo escolar
@stop

@section('description')
    Apertura y Cierre de Ciclos Escolares
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Ciclo Activo </a></li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2></h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <a type="button" id="agregarCicloEscolar" href="#" class="btn btn-block btn-primary">
                        <span class="glyphicon glyphicon-plus">Nuevo Ciclo Escolar</span>
                    </a>
                </div>
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!-- verificamos que la tabla no este vacia-->
            @if ($ciclos->isEmpty())
                <div>No hay ciclo escolar abierto</div>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Ciclo Escolar</th>
                            <th>Estado de Ciclo</th>   
                            <th>Accion</th>                     
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ciclos as $ciclo)
                            <tr>
                                 <td>{!! $ciclo->ciclo !!}</td>   
                                 <td>{!! $ciclo->estadoCiclo->estado !!}</td>      
                                <td>
                                <form method="post" action="{!! action('CiclosController@cerrarCiclo', $ciclo->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-edit">CERRAR</span></button>
                                    </div>                
                                </form>
                                </td>
                            
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                
            @endif
            
        </div>
    </div>
    @include ('ciclos.cicloModal');


<script>
$(document).ready(function() {
 $('div.alert').delay(5000).slideUp(300);

 $('#agregarCicloEscolar').click(function() 
{		
    document.forms["formulario"].action="{{route ('cicloStore')}}"; 
   
    $('#nombreModal').text("Aperturar Ciclo Escolar");
    $("#ciclo").modal({backdrop: 'static', keyboard: false});
            
    $.getScript("js/validaCiclo.js"); 
});


});
</script> 
@stop