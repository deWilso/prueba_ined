<div class="modal" fade id="grado">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
        
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">
                <span>&times;</span>
             </button>
             <h1 id="nombreModal"></h1>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="modal-body">
            <form class="form-horizontal" name="formulario" id="formulario"method="POST" action="#">
            {{csrf_field()}}
            @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
            <div class="box-body">
                <div class="form-group">
                    <label for="grado" class="col-sm-2 control-label">Ingrese el grado</label>

                    <div class="col-sm-6">
                        <input type="text" name ="nombreGrado" class="form-control" id="nombreGrado" placeholder="4to." value="">
                    </div>    
                </div>
                <div class="form-group">
                                            
                        <label for="carrera" id="lblcarrera" class="col-sm-2 control-label">Carrera</label>
                        <div class="col-sm-6"> 
                              <select class="form-control" name="carrera" id="carrera">
                                <option value="0">--Seleccione la Carrera del Grado--   </option>
                                @foreach($carreras as $carrera)
                                <option value="{{$carrera->id}}">{{$carrera->nombre}}</option>
                                @endforeach
                              </select>
                        </div>
                </div>
            </div>                 
            
            <div class="modal-footer" style="margin-top: 15px; text-align: center">
              <input type="button" id="cancelar" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
              <input type="submit" id="guardar" class="btn btn-primary" value="Guardar">
            </div>
        </form>
        </div>    
    </div>

</div>