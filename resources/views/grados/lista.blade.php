@extends('layouts.app')
@section('title')
    Grados 
@stop

@section('description')
    Gestion de grados
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>  Grados </a></li>
        <li class="active">Grados</li>
    </ol>
@stop
@section('content')
<div class="container col-md-12">
        <div class="panel panel-default pull-center" position="center">
            <div class="panel-heading">
                <h2>Lista de Grados</h2>
            </div>

            <div class="box-header">
                <div class="col-xs-3">
                    <a type="button" id="agregarGrado"href="#" class="btn btn-block btn-primary">
                        <span class="glyphicon glyphicon-plus"> Agregar Grado</span>
                    </a>
                </div>
                
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <!-- verificamos que la tabla no este vacia-->
            @if ($grados->isEmpty())
                <div>No hay registro de grados</div>
            @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Grado</th>
                            <th>Carrera</th>
                            <th>Accion</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($grados as $grado)
                            <tr>
                                 <td>{!! $grado->nombre !!}</td> 
                                 <td>{!! $grado->carreras->nombre !!}</td>     
                                
                                <td>
                                <!-- <form method="post" action="{!! action('GradosController@destroy', $grado->id) !!}" class="pull-right">
                                    {!! csrf_field() !!}
                                    <div>
                                    <button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                    </div>                
                                </form> -->

                                <a href="#" id="btnEditarGrado" data-value="{!! $grado->id !!}" class="btnEditarGrado"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $grados->render()!!}
            @endif
            
        </div>
    </div>
    @include ("grados.gradoModal");


<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
<script>
$(document).ready(function() {
$('div.alert').delay(5000).slideUp(300);
$('.btnEditarGrado').click(function() 
	{
	   
	   		var id = $(this).data("value"); 

			console.log(id);
			var form = $('#formulario');
			
			var urlTemp="{{route('gradoUpdate', ':grado_id')}}";
			var url = urlTemp.replace(':grado_id', id);
			document.forms["formulario"].action=url;		
			alert (url);
			$.ajax
			({
				type : 'get',
				url : '{{URL::to('gradoEdit')}}',
				data:{'id':id},
				success:function(data)
				{
				console.log(data);
				$('#carrera option[value="'+data[0].carrera_id+'"]').prop('selected', true);
				$("#nombreGrado").val(data[0].nombre);
				
				}
			});
			
	  		$('#nombreModal').text("Editar Grado");
			$("#grado").modal({backdrop: 'static', keyboard: false});
			$.getScript("js/validaGrado.js"); 

			
	 })

$('#agregarGrado').click(function() 
{		
    document.forms["formulario"].action="{{route ('gradoStore')}}"; 
   
    $('#nombreModal').text("Nuevo Grado");
    $("#grado").modal({backdrop: 'static', keyboard: false});
            
    $.getScript("js/validaGrado.js"); 
});

});
</script>
 @stop