$("#formulario").validate({ 
    errorClass: "my-error-class",
       validClass: "my-valid-class",
   rules:
   {
       pNombre: 
       {
           required: true,
           
       },
       action: "required",
       SNombre: 
       {
           required: true,
           
       },
       action: "required",
       pApellido:
       {
           required: true,
           
       },
       action: 'required',
      
       direccion:
       {
           required: true,
           
       },
       action:"required",
       tipo:
       {
           selectcheck: true,
           
       },
       action: "required",
       telefono:
       {
           required: true,
           minlength: 8,
           number:true,
       },
       
       action: "required",
       dpi: 
       {
           required: true,
           minlength: 13,
           number:true,
           
           
       },
       action: "required",
       email:
       {
           required: true,
           email:true,
       },
       action: "required",
       pass:
       {
           required: true,
           minlength: 8,
       },
       action: "required",
   },
   messages: 
   {
       tipo: {
           required: "Por favor seleccione un rol",
       },
       action: "por favor provea la informacion",
       pNombre: {
           required: "Por favor ingrese un nombre",
       },
       action: "por favor provea la informacion",
      
       pApellido: {
           required: "Por favor ingrese un apellido",
           
       
       },
       action: "por favor provea la informacion",

       dpi: {
        required: "Por favor ingrese el numero de identificacion",
        minlength: "almenos 13 caracteres",
        },
    action: "por favor provea la informacion",
    direccion: {
        required: "Por favor ingrese la direcccion",
        
    
    },
    action: "por favor provea la informacion",
    telefono: {
        required: "Por favor ingrese el numero de telefono",
        minlength: "almenos 8 caracteres",
        
    
    },
    action: "por favor provea la informacion",
    email: {
        required: "Por favor ingrese el correo electronico",
        mail: "utilice el formato juan@gmail.com",
        
    
    },
    action: "por favor provea la informacion",
    pass: {
        required: "escriba una contrasenia",
        minlength: "almenos 8 caracteres",
        
    
    },
    action: "por favor provea la informacion",
    
   }
});
jQuery.validator.addMethod('selectcheck', function (value) 
{
   return (value != '0');
}, "Campo requerido");