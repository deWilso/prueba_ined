$("#formularioEst").validate({ 
    errorClass: "my-error-class",
       validClass: "my-valid-class",
   rules:
   {
       carnet:
       {
        required:true,

       },
       action: "required",
       dpiEst: 
       {
           minlength: 13,
           
           
       },
       pNombreEst: 
       {
           required: true,
           
       },
       action: "required",
       
       pApellidoEst:
       {
           required: true,
           
       },
       action: 'required',
      
       direcionEst:
       {
           required: true,
           
       },
       action:"required",
       fechaNac:
       {
        required:true,
       }, 
       action: "required",
      telEstudiante:
       {
          
           minlength: 8,
           number:true,
       },
       
       genero:
       {
           selectcheck:true,
           required: true,
         
       },
       action: "required",
       grado:
       {    
           selectcheck:true,
           required: true,
          
       },
       action: "required",
       carrera:
       {    
           selectcheck:true,
           required: true,
          
       },
       action: "required",
   },
   messages: 
   {
       carnet: {
           required: "Por favor Ingrese el carnet",
       },
       action: "por favor provea la informacion",
       pNombreEst: {
           required: "Por favor ingrese un nombre",
       },
       action: "por favor provea la informacion",
      
       pApellidoE: {
           required: "Por favor ingrese un apellido",
           
       
       },
       action: "por favor provea la informacion",

       dpi: {
        required: "Por favor ingrese el numero de identificacion",
        minlength: "almenos 13 caracteres",
        },
    action: "por favor provea la informacion",
    direccion: {
        required: "Por favor ingrese la direcccion",
        
    
    },
    action: "por favor provea la informacion",
    telefono: {
        required: "Por favor ingrese el numero de telefono",
        minlength: "almenos 8 caracteres",
        
    
    },
    action: "por favor provea la informacion",
    email: {
        required: "Por favor ingrese el correo electronico",
        mail: "utilice el formato juan@gmail.com",
        
    
    },
    action: "por favor provea la informacion",
    pass: {
        required: "escriba una contrasenia",
        minlength: "almenos 8 caracteres",
        
    
    },
    action: "por favor provea la informacion",
    
   }
});
jQuery.validator.addMethod('selectcheck', function (value) 
{
   return (value != '0');
}, "Campo requerido");